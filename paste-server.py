#!/usr/bin/python3

from pwd import getpwuid
from socket import AF_UNIX, SOCK_DGRAM, socket
import atexit
import json
import os
import traceback

import boto3

SERVER_ADDRESS = "/tmp/paste-submission.sock"
WEB_USER = "www-data"
BUCKET = "ammonsmith-website"

if __name__ == '__main__':
    if getpwuid(os.getuid()).pw_name == WEB_USER:
        raise RuntimeError("Cannot be run as a CGI script")

    print("Binding to {}...".format(SERVER_ADDRESS))
    sock = socket(AF_UNIX, SOCK_DGRAM)
    sock.bind(SERVER_ADDRESS)
    atexit.register(lambda: os.unlink(SERVER_ADDRESS))

    print("Creating S3 client...")
    s3 = boto3.client('s3')

    def submit_paste(id, filename):
        s3.meta.client.upload_file(filename, BUCKET, '/paste/{}.json'.format(id))

        return {
            'id': id,
            'url': "https://paste.ammonsmith.me/{}".format(id),
        }

    REQUEST_HANDLERS = {
        'submit': submit_paste,
    }

    while True:
        try:
            print("Listening...")
            raw_request, addr = sock.recvfrom(4096)
            print("Got request from {}".format(addr))
            request = json.loads(raw_request, encoding='utf-8')

            handler = REQUEST_HANDLERS.get(request['action'])
            if handler is None:
                response = {
                    'error': "No such action: {}".format(request['action']),
                    'data': None,
                }
            else:
                try:
                    response = handler(**response['data'])
                except TypeError as err:
                    response = {
                        'error': str(err),
                        'data': None,
                    }
        except (KeyboardInterrupt, SystemExit):
            exit(1)
        except:
            traceback.print_exc()
