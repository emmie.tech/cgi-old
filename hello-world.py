#!/usr/bin/python3

HTML = """
<html>
<head>
    <title>Hello World</title>
</head>
<body>
    Hello, world!
</body>
"""

if __name__ == "__main__":
    print("Content-type: text/html\n\n")
    print(HTML)
