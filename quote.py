#!/usr/bin/python3

from socket import socket
import cgi
import re

# For debugging
import cgitb; cgitb.enable()

AUTHOR_REGEX = re.compile(r'\n    - (.+)')
RESPONSE_HTML = """\
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html" />
    <meta name="description" content="Quote of the Day" />
    <meta name="keywords" content="qotd,quote,of,the,day,ammon,smith" />
    <meta name="author" content="Ammon Smith">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Serif|Open+Sans|Roboto+Mono" />
    <link rel="stylesheet" href="/css/style.css" />

    <title>Quote of the Day{title_suffix}</title>
</head>

<body>
<div id="container">
    <div id="nav">
        [
        <a href="/">Home</a> |
        <a href="/blog/">Blog</a> |
        <a href="/cgi-bin/quote.py">Daily Quote</a> |
        <a href="/scp/">SCP</a> |
        <a href="/about.html">About</a> |
        <a href="https://jp.ammonsmith.me/">日本語</a>
        ]
    </div>

    <h2>Today's quote:</h2>
    <tt>{html_quote}</tt>
    <br>

    Provided by <a href="https://github.com/emmiegit/qotd">qotd</a>, port 17.
</div>
</body>
</html>
"""

def get_quote():
    sock = socket()
    sock.connect(('localhost', 17))
    sock.send(b'')
    quote = sock.recv(8192).decode('utf-8')
    sock.close()
    return quote[:-1]

if __name__ == '__main__':
    print("Content-Type: text/html\n\n")

    quote = get_quote()
    author = AUTHOR_REGEX.findall(quote)
    if author:
        title_suffix = ': {}'.format(author[0])
    else:
        title_suffix = ''

    html_quote = cgi.escape(quote) \
            .replace('\n', '<br>') \
            .replace('    ', '&nbsp;&nbsp;&nbsp;&nbsp;')

    print(RESPONSE_HTML.format(title_suffix=title_suffix, html_quote=html_quote))
