#!/usr/bin/python3

import cgi

# For debugging
import cgitb; cgitb.enable()

RESPONSE_HTML = """\
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html" />
    <meta name="description" content="Paste hosted on paste.ammonsmith.me" />
    <meta name="keywords" content="ammon,smith,paste,hosted" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Serif|Open+Sans|Roboto+Mono" />
    <link rel="stylesheet" href="/css/style.css" />

    <title>{title}</title>
</head>

<body>
<div id="container">
    <h2>{title}</h2>
    <tt>{body}</tt>
</div>
</body>
</html>
"""

if __name__ == '__main__':
    # TODO
    pass
