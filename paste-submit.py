#!/usr/bin/python3

from pprint import pformat
from random import getrandbits
from socket import AF_UNIX, SOCK_DGRAM, socket
import atexit
import base64
import cgi
import hashlib
import json
import tempfile
import traceback

# For debugging
import cgitb; cgitb.enable()

# Addresses
SERVER_ADDRESS = "/tmp/paste-submission.sock"
CLIENT_ADDRESS = "/tmp/paste-client-{}.sock".format(hex(getrandbits(64))[2:])

# HTML data
RESPONSE_HTML = """\
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html" />
    <meta http-equiv="refresh" content="0; URL={url}" />
    <meta name="description" content="Paste hosted on paste.ammonsmith.me" />
    <meta name="keywords" content="ammon,smith,paste,hosted,redirect" />

    <title>{title}</title>
</head>

<body>{body}</body>
</html>
"""

ERROR_HTML = """\
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html" />
    <meta name="description" content="Error submitting paste" />
    <meta name="keywords" content="ammon,smith,paste,error,submit,cgi" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Serif|Open+Sans|Roboto+Mono" />
    <link rel="stylesheet" href="/css/style.css" />

    <title>Error submitting paste</title>
</head>

<body>
<div id="container">
    <h2>Error</h2> <br />
    {html}

    <p>Click <a href="https://paste.ammonsmith.me/">here</a> to return.</p>
</div>
</body>
</html>
"""

SEND_TO_SOCKET = """\
Error sending data to socket: <br />
<tt>
{}
</tt>
"""

def preserve_whitespace(s):
    return s.replace('\n', ' <br />').replace('  ', '&nbsp;&nbsp;')

if __name__ == '__main__':
    print("Content-Type: text/html\n\n")

    form = cgi.FieldStorage()
    if 'content' not in form:
        # Empty paste
        print(RESPONSE_HTML.format(
            url="https://paste.ammonsmith.me",
            title="Empty paste submitted",
            body="",
        ))
        exit()

    title = form['title'].value if 'title' in form else "Untitled Paste"
    content = form['content'].value

    hasher = hashlib.sha512()
    hasher.update(title.encode('utf-8'))
    hasher.update(content.encode('utf-8'))
    hash_id = hasher.hexdigest()[8:28]

    obj = {
        'id': hash_id,
        'title': title,
        'content': content,
    }

    # Write paste data to temp file
    fh = tempfile.NamedTemporaryFile()
    fh.write(json.dumps(obj).encode('utf-8'))
    atexit.register(lambda: fh.close())

    # Create UNIX Domain socket
    sock = socket(AF_UNIX, SOCK_DGRAM)
    sock.bind(CLIENT_ADDRESS)
    sock.settimeout(0.5)
    atexit.register(lambda: os.unlink(CLIENT_ADDRESS))

    # Submit paste to S3
    request = {
        'action': 'submit',
        'data': {
            'id': hash_id,
            'filename': fh.name,
        },
    }

    try:
        raw_request = json.dumps(request).encode('utf-8')
        sock.sendto(raw_request, SERVER_ADDRESS)
        raw_response, _ = sock.recvfrom(4096)
        response = json.loads(raw_response, encoding='utf-8')
    except:
        html = SEND_TO_SOCKET.format(preserve_whitespace(traceback.format_exc()))
        print(ERROR_HTML.format(html=html))
        exit()

    # Validate success
    if response['error'] is not None:
        html = "Error submitting paste: {}".format(preserve_whitespace(response['error']))
        print(ERROR_HTML.format(html=html))
        exit()

    url = response['data']['url']
    print(RESPONSE_HTML.format(title=title, body=content, url=url))
