#!/usr/bin/python3

import cgi
import os
import re
import sys
import time

from bs4 import BeautifulSoup
from urllib.request import urlopen

# For debugging
import cgitb; cgitb.enable()

CURRENT_SERIES = 8
SCP_SERIES_REGEX = re.compile(r'/scp-[0-9]{3,4}')
CACHE_FILE = '/tmp/_scp-series-cache.html'
CACHE_EXPIRATION = 120

RESPONSE_HTML = """\
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html" />
    <meta name="description" content="Shows the number of remaining slots in each SCP Series" />
    <meta name="keywords" content="scp,series,slot,counter,empty,available,ammon,smith" />
    <meta name="author" content="Ammon Smith">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Serif|Open+Sans|Roboto+Mono" />
    <link rel="stylesheet" href="/css/style.css" />

    <title>SCP Series Slot Counter</title>

    <style>
        .series-name {{
            display: inline-block;
            font-weight: bold;
            width: 5em;
        }}

        .series-count {{
            display: inline-block;
            text-align: right;
            width: 5em;
        }}
    </style>
</head>

<body>
<div id="container">
    <div id="nav">
        [
        <a href="/">Home</a> |
        <a href="/blog/">Blog</a> |
        <a href="/cgi-bin/quote.py">Daily Quote</a> |
        <a href="/scp/">SCP</a> |
        <a href="/about.html">About</a> |
        <a href="https://jp.ammonsmith.me/">日本語</a>
        ]
    </div>

    <h2>SCP Series Slot Counts</h2>
    <p>
        <div>
            <span class="series-name">
                <a href="https://scp-wiki.wikidot.com/scp-series" target="_blank">Series I</a>
            </span>
            {series1}
        </div>

        <div>
            <span class="series-name">
                <a href="https://scp-wiki.wikidot.com/scp-series-2" target="_blank">Series II</a>
            </span>
            {series2}
        </div>

        <div>
            <span class="series-name">
                <a href="https://scp-wiki.wikidot.com/scp-series-3" target="_blank">Series III</a>
            </span>
            {series3}
        </div>

        <div>
            <span class="series-name">
                <a href="https://scp-wiki.wikidot.com/scp-series-4" target="_blank">Series IV</a>
            </span>
            {series4}
        </div>

        <div>
            <span class="series-name">
                <a href="https://scp-wiki.wikidot.com/scp-series-5" target="_blank">Series V</a>
            </span>
            {series5}
        </div>

        <div>
            <span class="series-name">
                <a href="https://scp-wiki.wikidot.com/scp-series-6" target="_blank">Series VI</a>
            </span>
            {series6}
        </div>

        <div>
            <span class="series-name">
                <a href="https://scp-wiki.wikidot.com/scp-series-7" target="_blank">Series VII</a>
            </span>
            {series7}
        </div>

        <div>
            <span class="series-name">
                <a href="https://scp-wiki.wikidot.com/scp-series-8" target="_blank">Series VIII</a>
            </span>
            {series8}
        </div>
    </p>

    Provided by <a href="https://github.com/emmiegit/scripts/blob/master/scp-slots.py">scp-slots.py</a>.
</div>
</body>
</html>
"""

def count_slots(url):
    with urlopen(url) as response:
        data = response.read()

    empty_slots = 0
    total_slots = 0
    scps = set()

    soup = BeautifulSoup(data, 'html.parser')
    for link in soup.select('.series a'):
        href = link.get('href')
        if href is None or SCP_SERIES_REGEX.match(href) is None:
            continue

        if href in scps:
            continue

        scps.add(href)
        total_slots += 1
        if link.get('class') == ['newpage']:
            empty_slots += 1

    return empty_slots, total_slots

def get_series_url(number):
    if number == 1:
        return 'https://scp-wiki.wikidot.com/scp-series'
    else:
        return 'https://scp-wiki.wikidot.com/scp-series-{}'.format(number)

def cache_expired():
    try:
        statinfo = os.stat(CACHE_FILE)
    except OSError:
        return True

    time_since = time.time() - statinfo.st_mtime
    return time_since > CACHE_EXPIRATION

def cache_save(html):
    with open(CACHE_FILE, 'wb') as file:
        data = html.encode('utf-8')
        file.write(data)

if __name__ == '__main__':
    print('Content-Type: text/html\n\n')

    if not cache_expired():
        with open(CACHE_FILE, 'rb') as file:
            html = file.read().decode('utf-8')

        print(html)
        exit()

    params = {}
    for series in range(1, CURRENT_SERIES + 1):
        url = get_series_url(series)
        empty, total = count_slots(url)
        percent = (total - empty) / total * 100
        slot_info = '<span class="series-count">{} / {}</span> ({:.1f}% filled)'.format(empty, total, percent)
        params['series{}'.format(series)] = slot_info

    html = RESPONSE_HTML.format(**params)
    cache_save(html)
    print(html)
